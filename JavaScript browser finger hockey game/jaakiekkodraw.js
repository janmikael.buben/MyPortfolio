// Function for drawing a paddle
function drawPaddle() {
  ctx.beginPath();
  ctx.rect(paddleX, canvas.height-paddleHeight, paddleWidth, paddleHeight);
  ctx.fillStyle = "brown";
  ctx.fill();
  ctx.closePath();
}

//Creates an array for checking status of "back"
var goalies = [];
for (var ds = 0; ds < goaliesColumnCount; ds++) {
  goalies[ds] = [];
  for(var ws=0; ws<goaliesRowCount; ws++) {
    goalies[ds][ws] = { x: 0, y: 0, status: 1}
  }
}

// Function for drawing the line behind the goal for knockback
function drawGoaliees() {
  for(var ds=0; ds<goaliesColumnCount; ds++) {
    for(var ws=0; ws<brickRowCount; ws++) {
      if(goalie[ds][ws].status == 1) {
        var goalieXs = (ws*(goaliesWidth+goaliesPadding))+goaliesOffsetLeft;
        var goalieYs = (ds*(goaliesHeight+goaliesPadding))+goaliesOffsetTop;
        goalies[ds][ws].x = goalieXs;
        goalies[ds][ws].y = goalieYs;
        ctx.beginPath();
        ctx.rect(goalieXs, goalieYs, goaliesWidth, goaliesHeight);
        ctx.fillStyle = "orange";
        ctx.fill();
        ctx.closePath();
      }
    }
  }
}

// Syntax for checking the goalkeepers status
var goalie = [];
for (var d = 0; d < goalieColumnCount; d++) {
  goalie[d] = [];
  for(var w=0; w<goalieRowCount; w++) {
    goalie[d][w] = { x: 0, y: 0, status: 1}
  }
}

// Draw function for drawing the goalkeeper
function drawGoaliee() {
  for(var d=0; d<goalieColumnCount; d++) {
    for(var w=0; w<brickRowCount; w++) {
      if(goalie[d][w].status == 1) {
        var goalieX = (w*(goalieWidth+goaliePadding))+m;
        var goalieY = (d*(goalieHeight+goaliePadding))+q;
        goalie[d][w].x = goalieX;
        goalie[d][w].y = goalieY;
        ctx.beginPath();
        ctx.rect(goalieX, goalieY, goalieWidth, goalieHeight);
        ctx.fillStyle = "green";
        ctx.fill();
        ctx.closePath();
      }
    }
  }
}

// Checks status of bricks
var bricks = []
for(var c=0; c<brickColumnCount; c++) {
    bricks[c] = [];
    for(var r=0; r<brickRowCount; r++) {
      bricks[c][r] = { x: 0, y: 0, status: 1};
    }
}

// Draws the bricks
function drawBricks() {
  for(var c=0; c<brickColumnCount; c++) {
      for(var r=0; r<brickRowCount; r++) {
        if(bricks[c][r].status == 1) {
          var brickX = (r*(brickWidth+brickPadding))+brickOffsetLeft;
          var brickY = (c*(brickHeight+brickPadding))+brickOffsetTop;
          bricks[c][r].x = brickX;
          bricks[c][r].y = brickY;
          ctx.beginPath();
          ctx.rect(brickX, brickY, brickWidth, brickHeight);
          ctx.fillStyle = "#0095DD";
          ctx.fill();
          ctx.closePath();
      }
    }
  }
}


// Checks for collision between goal and the puck, resets puck location on goal
function collisionDetection() {
  for(var c=0; c<brickColumnCount; c++) {
    for(var r=0; r<brickRowCount; r++) {
      var b = bricks[c][r];
      if(x > b.x && x < b.x+brickWidth && y > b.y && y < b.y+brickHeight) {
          dy = -dy;
          score++;
          x = canvas.width * 0.5;
          y = canvas.height * 0.5;
          dy = 0;
          dx = 0;
          if(score == 5){
            alert("YOU WIN!");
            document.location.reload();
        }
      }
    }
  }
}
// Checks collision with backwall and resets puck on hit alongside an error message
function goalieDetections() {
  for(var ds=0; ds<goaliesColumnCount; ds++) {
    for(var ws=0; ws<goaliesRowCount; ws++) {
      var gs = goalies[ds][ws];
      if(x > gs.x && x < gs.x+goaliesWidth && y > gs.y && y < gs.y+goaliesHeight) {
        dy = -dy;
        x = canvas.width * 0.5;
        y = canvas.height * 0.5;
        dx = 0;
        dy = 0;
        alert("You missed!");
      }
    }
  }
}

// Function for detecting collision with the goalkeeper
function goalieDetection() {
  for(var d=0; d<goalieColumnCount; d++) {
    for(var w=0; w<goalieRowCount; w++) {
      var g = goalie[d][w];
      if(x > g.x && x < g.x+goalieWidth && y > g.y && y < g.y+goalieHeight) {
        dy = -dy;
        x = canvas.width * 0.5;
        y = canvas.height * 0.5;
        dx = 0;
        dy = 0;
        alert("Your shot got blocked!");
      }
    }
  }
}



// Function to draw the score of the player
function drawScore() {
  ctx.font = "16px Arial";
  ctx.fillStyle = "#0095DD";
  ctx.fillText("Score: "+score, 8, 20);
}
// Draws the puck
function drawPuck() {
    ctx.beginPath();
    ctx.arc(x, y, puckRadius, 0, Math.PI*2);
    ctx.fillStyle = "black";
    ctx.fill();
    ctx.closePath();
}

// Draws the goalzone
function drawGoalie() {
  ctx.beginPath();
  ctx.arc(z, p, goalieRadius, 0, Math.PI*2);
  ctx.fillStyle = "9999ff";
  ctx.fill();
  ctx.closePath();
}

function drawgoalred() {
    ctx.beginPath()
    ctx.beginPath();
    ctx.arc(z, p, goalieRadius + 3, 0, Math.PI*2);
    ctx.fillStyle = "red";
    ctx.fill();
    ctx.closePath();
  }

// Draws the goal
function drawGoal() {
   ctx.beginPath();
   ctx.rect(z-goalWidth/2, p-goalHeight, goalWidth, goalHeight);
   ctx.fillStyle = "red";
   ctx.fill();
   ctx.closePath();
}
// Draws a light red circle area in the middle
function drawZone() {
  ctx.beginPath();
  ctx.arc(canvas.width/2, canvas.height/2, 35, 0, Math.PI*2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a white circle to the middle of the canvas
function drawZonetwo() {
  ctx.beginPath();
  ctx.arc(canvas.width/2, canvas.height/2, 30, 0, Math.PI*2);
  ctx.fillStyle = "white";
  ctx.fill();
  ctx.closePath();
}
// Draws a long thin light red line through the middle of the canvas
function drawZonethree() {
  ctx.beginPath();
  ctx.rect(0, 248, 300, 5);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a small black circle in the middle of the canvas
function drawZonefour() {
  ctx.beginPath();
  ctx.arc(canvas.width/2, 250, 6, 0, Math.PI*2);
  ctx.fillStyle = "black";
  ctx.fill();
  ctx.closePath();
}
// Paints the whole canvas white with a white drawing rectangle
function drawZonefive() {
  ctx.beginPath();
  ctx.rect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle = "white";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red circle in the top right of the canvas
function drawZonesix() {
  ctx.beginPath();
  ctx.arc(235, 125, 45, 0, Math.PI*2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a white circle in the top right side on top of the light red circle
function drawZoneseven() {
  ctx.beginPath();
  ctx.arc(235, 125, 40, 0, Math.PI*2);
  ctx.fillStyle = "white";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red circle in the top left side of the canvas
function drawZoneeight() {
  ctx.beginPath();
  ctx.arc(60, 125, 45, 0, Math.PI*2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a white circle in the top left side of canvas on top of the light red circle
function drawZonenine() {
  ctx.beginPath();
  ctx.arc(60, 125, 40, 0, Math.PI*2);
  ctx.fillStyle = "white";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red circle in the bottom right of the canvas
function drawZoneten() {
  ctx.beginPath();
  ctx.arc(235, 375, 45, 0, Math.PI*2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a white circle in the bottom right of the canvas on top of the light red circle
function drawZoneeleven() {
  ctx.beginPath();
  ctx.arc(235, 375, 40, 0, Math.PI*2);
  ctx.fillStyle = "white";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red circle in the bottom left side of the canvas
function drawZonetwelve() {
  ctx.beginPath();
  ctx.arc(60, 375, 45, 0, Math.PI*2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a white circle in the bottom left side of the canvas on top of the light red circle
function drawZonethirteen() {
  ctx.beginPath();
  ctx.arc(60, 375, 40, 0, Math.PI*2);
  ctx.fillStyle = "white";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red dot on the bottom left light red circle on the canvas
function drawZonefourteen() {
  ctx.beginPath();
  ctx.arc(60, 375, 7, 0, Math.PI*2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red dot on the bottom right light red circle on the canvas
function drawZonefifteen() {
  ctx.beginPath();
  ctx.arc(235, 375, 7, 0, Math.PI*2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red dot on the top left light red circle on the canvas
function drawZonesixteen() {
  ctx.beginPath();
  ctx.arc(60, 125, 7, 0, Math.PI*2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red dot on the top right light red circle on the canvas
function drawZoneseventeen() {
  ctx.beginPath();
  ctx.arc(235, 125, 7, 0, Math.PI*2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a horizontal light red line inside the top right light red circle to the bottom right of the light red dot
function drawZoneeighteen() {
  ctx.beginPath();
  ctx.rect(240, 134, 15, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a vertical light red line inside the top right light red circle to the bottom right of the light red dot
function drawZonenineteen() {
  ctx.beginPath();
  ctx.rect(240, 134, 2, 15);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a vertical light red line inside the top right light red circle to the bottom left side of the light red dot
function drawZonetwenty() {
  ctx.beginPath();
  ctx.rect(230, 134, 2, 15);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a horizontal light red line inside the top right light red circle to the bottom left side of the light red  dot
function drawZonetwentyone() {
  ctx.beginPath();
  ctx.rect(215, 134, 15, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a vertical light red line to the top right light red circle to the top left side of the light red dot
function drawZonetwentytwo() {
  ctx.beginPath();
  ctx.rect(230, 102, 2, 15);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a horizontal light red line to the top right light red circle to the top left side of the light red dot
function drawZonetwentythree() {
  ctx.beginPath();
  ctx.rect(215, 115, 15, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a horizontal light red line to the top right light red circle to the top right side of the light red dot
function drawZonetwentyfour() {
  ctx.beginPath();
  ctx.rect(240, 115, 15, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a vertical light red line to the top right light red circle to the top right side of the light red dot
function drawZonetwentyfive() {
  ctx.beginPath();
  ctx.rect(240, 102, 2, 15);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a vertical line on the top left light red circle bottom right of the light red  dot
function drawZonetwentysix() {
  ctx.beginPath();
  ctx.rect(65, 134, 2, 15)
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a horizontal line on the top left light red circle to the right bottom side of the light red dot
function drawZonetwentyseven() {
  ctx.beginPath();
  ctx.rect(65, 134, 15, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a horizontal line ot the top left light red circle to the left bottom side of the light red dot
function drawZonetwentyeight() {
  ctx.beginPath();
  ctx.rect(40,134, 15, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a vertical line to the top left light red circle to the left bottom side of the light red dot
function drawZonetwentynine() {
  ctx.beginPath();
  ctx.rect(55, 134, 2, 15);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a horizontal line to the top left light red circle to the top right side of the light red dot
function drawZonethirty() {
  ctx.beginPath();
  ctx.rect(65, 116, 15, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a vertical line to the top left light red circle to the top right side of the light red dot
function drawZonethirtyone() {
  ctx.beginPath();
  ctx.rect(65, 101, 2, 15);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a vertical line to the top left light red circle to the top left side of the light red dot
function drawZonethirtytwo() {
  ctx.beginPath();
  ctx.rect(55, 101, 2, 17);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a horizontal line to the top light red left circle to the top left side of the light red dot
function drawZonethirtythree() {
  ctx.beginPath();
  ctx.rect(40, 116, 15, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a horizontal line to the bottom right side light red circle to the bottom right side of the light red dot
function drawZonethirtyfour() {
  ctx.beginPath();
  ctx.rect(240, 384, 15, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a vertical line to the bottom right side light red circle to the bottom right side of the light red dot
function drawZonethirtyfive() {
  ctx.beginPath();
  ctx.rect(240, 384, 2, 15);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a vertical light red line to the bottom right side light red circle to the bottom left side of the light red dot
function drawZonethirtysix() {
  ctx.beginPath();
  ctx.rect(230, 384, 2, 15);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a horizontal light red line to the bottom right side light red circle to the bottom left side of the light red dot
function drawZonethirtyseven() {
  ctx.beginPath();
  ctx.rect(215, 384, 15, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a horizontal light red line to the bottom right side light red cirle to the top right side of the light red dot
function drawZonethirtyeight() {
  ctx.beginPath();
  ctx.rect(240, 366, 15, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a vertical light red line to the bottom right side light red circle to the top right side of the light red dot
function drawZonethirtynine() {
  ctx.beginPath();
  ctx.rect(240, 351, 2, 15);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a horizontal light red line to the bottom right side light red circle to the top left side of the light red dot
function drawZoneforty() {
  ctx.beginPath();
  ctx.rect(215, 366, 15, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a vertical light red line to the bottom right side light red circle to the top left side of the light red dot
function drawZonefortyone() {
  ctx.beginPath();
  ctx.rect(230, 351, 2, 17);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a horizontal light red line to the bottom left side light red circle to the bottom right side of the light red dot
function drawZonefortytwo() {
  ctx.beginPath();
  ctx.rect(65, 384, 15, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a vertical light red line to the bottom left side light red circle to the bottom right side of the light red dot
function drawZonefortythree() {
  ctx.beginPath();
  ctx.rect(65, 384, 2, 15);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a vertical light red line to the bottom left side light red circle to the bottom left side of the light red dot
function drawZonefortyfour() {
  ctx.beginPath();
  ctx.rect(55, 384, 2, 15);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a horizontal light red line to the bottom left side light red circle to the bottom left side of the light red dot
function drawZonefortyfive() {
  ctx.beginPath();
  ctx.rect(40, 384, 15, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a horizontal light red line to the bottom left side light red circle to the top right side of the light red dot
function drawZonefortysix() {
  ctx.beginPath();
  ctx.rect(65, 366, 15, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a vertical light red line to the bottom left side light red circle to the top right side of the light red dot
function drawZonefortyseven() {
  ctx.beginPath();
  ctx.rect(65, 351, 2, 15);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a horizontal light red line to the bottom left side light red circle to the top left side of the light red dot
function drawZonefortyeight() {
  ctx.beginPath();
  ctx.rect(40, 366, 15, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a vertical light red line to the bottom left side light red circle to the top left side of the light red dot
function drawZonefortynine() {
  ctx.beginPath();
  ctx.rect(55, 351, 2, 17);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red line sticking out of the right side of the right top light red circle from the right side of the circle, top line
function drawZonefifty() {
  ctx.beginPath();
  ctx.rect(275, 115, 12, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red line sticking out of the right side of the right top light red circle from the right side of the circle, bottom line
function drawZonefiftyone() {
  ctx.beginPath();
  ctx.rect(275, 134, 12, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red line sticking out of the left side of the top right light red circle from the left side, upper line
function drawZonefiftytwo() {
  ctx.beginPath();
  ctx.rect(184, 115, 12, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red line sticking out of the left side of the top right light red circle, lower line
function drawZonefiftythree() {
  ctx.beginPath();
  ctx.rect(184, 134, 12, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red line sticking out from the right side of the top left light red circle, top line
function drawZonefiftyfour() {
  ctx.beginPath();
  ctx.rect(100, 116, 12, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red line sticking out from the right side of the top left light red circle, bottom line
function drawZonefiftyfive() {
  ctx.beginPath();
  ctx.rect(100, 134, 12, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red line sticking out from the left side of the top left light red circle, top line
function drawZonefiftysix() {
  ctx.beginPath();
  ctx.rect(8, 116, 12, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red line sticking out from the left side of the top left light red circle, bottom line
function drawZonefiftyseven() {
  ctx.beginPath();
  ctx.rect(8, 134, 12, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red line sticking out from the right side of the bottom left light red circle, top line
function drawZonefiftyeight() {
  ctx.beginPath();
  ctx.rect(100, 366, 12, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red line sticking out from the rightside of the bottom left light red circle, bottom line
function drawZonefiftynine() {
  ctx.beginPath();
  ctx.rect(100, 384, 12, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a red line line sticking out from the left side of the bottom left light red circle, top line
function drawZonesixty() {
  ctx.beginPath();
  ctx.rect(8, 366, 12, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red line sticking out from the left side of the bottom left light red circle, bottom line
function drawZonesixtyone() {
  ctx.beginPath();
  ctx.rect(8, 384, 12, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red line sticking out from the right side of the bottom right light red circle, top line
function drawZonesixtytwo() {
  ctx.beginPath();
  ctx.rect(275, 366, 12, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red line sticking out from the right side of the bottom right light red circle, bottom line
function drawZonesixtythree() {
  ctx.beginPath();
  ctx.rect(275, 384, 12, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red line sticking out from the left side of the bottom right light red circle, top line
function drawZonesixtyfour() {
  ctx.beginPath();
  ctx.rect(183, 366, 12, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red line sticking out from the left side of the bottom right light red circle, bottom line
function drawZonesixtyfive() {
  ctx.beginPath();
  ctx.rect(183, 384, 12, 2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a thin light blue line across the bottom side of the arena
function drawZonesixtysix() {
  ctx.beginPath()
  ctx.rect(0, 310, 300, 5);
  ctx.fillStyle = "4d4dff";
  ctx.fill();
  ctx.closePath();
}
// Draws a thin light blue line across the top side of the arena
function drawZonesixtyseven() {
  ctx.beginPath();
  ctx.rect(0, 190, 300, 5);
  ctx.fillStyle = "4d4dff";
  ctx.fill();
  ctx.closePath()
}
// Draws a light red dot next to the top light blue line on the right side
function drawZonesixtyeight() {
  ctx.beginPath();
  ctx.arc(235, 205, 7, 0, Math.PI*2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red dot next to the top light blue line to the left side
function drawZonesixtynine() {
  ctx.beginPath();
  ctx.arc(60, 205, 7, 0, Math.PI*2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red dot next to the bottom 4d4dff line to the left side
function drawZoneseventy() {
  ctx.beginPath();
  ctx.arc(60, 295, 7, 0, Math.PI*2);
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
// Draws a light red dot next to the bottom light blue line to the right side
function drawZoneseventyone() {
  ctx.beginPath();
  ctx.arc(235, 295, 7, 0, Math.PI*2)
  ctx.fillStyle = "b34d4d";
  ctx.fill();
  ctx.closePath();
}
