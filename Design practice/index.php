<?php
  include("https.php")
?>
<!DOCTYPE html>
<html lang="fi-FI">
<head>
  <title>Louhosdigital kotitehtävä</title>
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="main.css">
  <meta charset="UTF-8"/>
</head>
<body>

  <div id="wrapper">
    <nav class="col-12">
      <div class="navbarlogo">
        <img src="logo.svg" alt="logo">
      </div>
      <div class="links">
        <a href="#">Menu item 1</a>
        <a href="#">Menu item 2</a>
        <a href="#">Menu item 3</a>
      </div>
    </nav>
    <div class="imagediv col-12">
      <h1>Hurray! I can code!</h1>
    </div>

    <div class="box col-2">
      <h1>Title goes here</h1>
      <p>And text goes here</p>
      <button class="button">Button</button>
    </div>
    <div class="box col-2">
      <h1>Title goes here</h1>
      <p>And text goes here</p>
      <button class="button">Button</button>
    </div>
    <div class="box col-2">
      <h1>Title goes here</h1>
      <p>And text goes here</p>
      <button class="button">Button</button>
    </div>

    <div id="footer" class="col-12">
      <div class="footerimage">
        <img src="logo.svg" alt="logo">
      </div>
      <section class="bfooter">
        <p>This is the footer of the template!</p>
        <p id="copyrighttext">Copyright 2020</p>
      </section>
    </div>
  </div>
</body>
</html>
