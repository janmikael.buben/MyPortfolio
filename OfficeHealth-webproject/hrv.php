<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$data = array(

        array("day"=> "04-21",
        "value"=>70.6,
        "stressIndex"=> 5.00),
        array("day"=> "04-22",
        "value"=> 65.5,
      "stressIndex"=> 5.00),
        array("day"=> "04-23",
        "value"=> 50.0,
      "stressIndex"=> 5.00),
        array("day"=> "04-24",
        "value"=> 40.0,
      "stressIndex"=> 5.00),
        array("day"=> "04-25",
        "value"=> 77.0,
      "stressIndex"=> 5.00),
        array("day"=> "04-26",
        "value"=> 60.4,
      "stressIndex"=> 5.00),
        array("day"=> "04-27",
        "value"=> 38.5,
        "stressIndex"=> 5.00),
        array("day"=> "04-28",
        "value"=> 60.5,
        "stressIndex"=> 5.00),
        array("day"=> "04-29",
        "value"=> 66.5,
        "stressIndex"=> 5.00)
      );
        /*
    }, {
        "year": "1985",
        "value": -0.037
    }, {
        "year": "1986",
        "value": 0.03
    }, {
        "year": "1986",
        "value": 0.03
    },
    {
        "year": "1986",
        "value": 0.03
    },
    {
        "year": "1986",
        "value": 0.03
    },
    {
        "year": "1986",
        "value": 0.03
    }];
*/
  echo (json_encode($data));

?>
