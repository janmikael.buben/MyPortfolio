<?php
// Session aloitus, tietokanta ja HTTPS -protokolla
     session_start(); 
     include_once('../config/config.php');
     include_once('../config/https.php');
?>
<!DOCTYPE html>
  <html lang="fi-FI">
    <head>
      <title>Merkintä </title>
      <link rel="preconnect" href="https://fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="../styles/entryStyles.css">
      <script src="https://kit.fontawesome.com/cd048a1463.js" crossorigin="anonymous"></script>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta charset="UTF-8"/>
  </head>
<body>

<!--- Kyselyn vastauspohja --->
<div id="wrapper"> 
  <div class="col-2">
    <div class="entryForm">
      <fieldset>
       <legend>Luo uusi merkintä</legend>
         <form method="POST" id="entry" >
          <p>Olotila &nbsp; <i class="far fa-question-circle">
         <span class="condi">Kuvaile päivän olotilaasi</span>
          </i>
         </p>
  <select class="dropdown" name="condition">
      <option>Todella hyvä</option>
      <option>Hyvä</option>
      <option>Ok</option>
      <option>Huono</option>
      <option>Todella huono</option>
  </select>
    <p>Unen määrä (h) &nbsp; <i class="far fa-question-circle">
      <span class="sleep">Kerro kuinka paljon olet nukkunut päivän aikana.</span>
      </i>
    </p>
      <input type="number" name="sleepAmount" placeholder="Anna unen määrä" min="0" max="24" />
        <p>Minkälaista liikuntaa olet tehnyt tänään: &nbsp; <i class="far fa-question-circle">
          <span class="activ">Kerro miten olet liikkunut tänään.</span>
          </i>
           </p>
          <select name="activity" >
            <option>Kävely</option>
            <option>Lenkkeily</option>
            <option>Kuntosali</option>
            <option>Uinti</option>
            <option>Pyöräily</option>
            <option>En mitään</option>
            <option>Muu</option>
          </select>
          <br>
          <input type="submit" name="submitEntry" value="Luo merkintä" />
          <input type="submit"  name="back" value="Takaisin" />
        </form>
      </fieldset>
    </div>
  </div>
</div>

<?php
  // käyttäjän kysely
    $data1['name1'] = $_SESSION['suserName'];
    $sql1 = "SELECT userID FROM officehealth_user where userName = :name1";
    $kysely1=$DBH->prepare($sql1); // ottaa yhteyden tietokannassa olevaan tauluun (officehealth_user)
    $kysely1->execute($data1);
    $tulos1=$kysely1->fetch(); 
    $currentID = $tulos1[0];
    ?>

<?php 
    // Kyselyn tulostus
     if (isset($_POST['submitEntry'])) {
         try {
           // alustaa input arvot ja käyttäjän
            $data['condition'] = $_POST['condition'];
            $data['sleep'] = $_POST['sleepAmount'];
            $data['activity'] = $_POST['activity'];
            $data['userData'] = $currentID;

            // Tallentaa ja lisää dataa officehealth_user_entry -tauluun.
            $sql = "INSERT INTO officehealth_user_entry (condition2, sleep, activity, userPersonID) VALUES (:condition, :sleep, :activity, :userData);";
            $entry = $DBH->prepare($sql);
            $entry->execute($data);
    } catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
     }
  }
?>

<?php
// Kun "Takaisin" -nappia painettu paluu takaisin päiväkirja -sivulle.
  if(isset($_POST['back'])){
    header("Location: ../diary.php");
  }
  ?>

<?php
// Merkinnän teon jälkeen popup -ilmoitus ja paluu päiväkirja -sivulle
  if(isset($_POST['submitEntry'])){
    echo '<script>alert("Merkintä tallennettu!")
    window.location.href="../diary.php"; 
    </script>';
    }
  ?>
</body>
</html>

