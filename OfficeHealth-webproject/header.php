<?php
include_once("config/https.php");
include_once("config/config.php");
session_start();
?>
<!DOCTYPE html>
<html lang="fi-FI">
<head>
  <title>OfficeHealth kirjautuminen </title>
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://kit.fontawesome.com/3a9c0f3274.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="styles/loginStyles.css">
  <link rel="stylesheet" href="styles/footerStyles.css">
  <link rel="stylesheet" href="styles/main.css">
  <meta charset="UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<!-- header login ja register sivuille -->
<body>
  <div id="wrapper">
    <nav class="col-12">
      <div class="navbarlogo">
        <img class="logo" alt="logo" src="images/logo3.2.svg">
      </div>
    </nav>
