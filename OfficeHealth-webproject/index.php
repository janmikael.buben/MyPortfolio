<?php
session_start();
if(!isset($_SESSION['sloggedIn'])){
  header('Location: login.php');
}
?>
<!DOCTYPE html>
<html lang="fi-FI">
<head>
  <title>Office health etusivu</title>
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="styles/main.css">
  <link rel="stylesheet" href="styles/footerStyles.css">
  <link rel="stylesheet" href="styles/indexStyles.css">
  <script src="https://kit.fontawesome.com/3a9c0f3274.js" crossorigin="anonymous"></script>
  <meta charset="UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
  <div id="wrapper">
    <nav class="col-12">
      <div class="navbarlogo">
        <a href="index.php">
        <img class="logo" alt="logo" src="images/logo3.2.svg">
        </a>
      </div>
      <div id="myLinks" class="links">
        <a href="index.php" class="bolded">Etusivu</a>
        <a href="diary.php">Päiväkirja</a>
        <a href="calendar.php">Kalenteri</a>
        <a href="profile.php">Profiili</a>
        <a href="logout.php">Kirjaudu ulos</a>
      </div>
      <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars"></i>
  </a>
    </nav>
    <script>
    function myFunction() {
    var x = document.getElementById("myLinks");
    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "block";
    }
  }
    </script>
    <div class="imagediv col-12">
      <?php
      //Etusivulle tulostuu tervetuloa toivotus kirjautuneelle käyttäjälle
      $name = $_SESSION['suserName'];
      echo "<h1>Tervetuloa " . $name . "!</h1>";
      ?>
    </div>

    <div class="textcontent col-5">
      <h1>Aloitus:</h1>
      <p>
        Tervetuloa käyttämään Office Healthia! Office Healthin tarkoituksena on auttaa sinua seuraamaan terveyttäsi sekä olotilaasi paremmin, ja mahdollisesti parantamaan
        elämäntyyliäsi tulevaisuuttasi varten.
      </p>
      <p>
      Suosittelemme näyttämään Office Healthista saadut tulokset mahdollisella terveydenhuoltoasemalla, tai työterveyskäynnillä, jotta terveydenhuollon ammattilaiset voisivat
      antaa mahdollisesti paremman analyysin tilanteeseesi, ja suositella jatkotoimenpiteitä.
      </p>
      <p>
        Aloittaaksesi, siirryttehän "Päiväkirja"-sivulle, jossa voitte luoda ensimmäisen päiväkirjamerkintänne. Löydätte Päiväkirja-sivun painamalla
        "Päiväkirja"-nappia sivun ylälaidasta.
      </p>
    </div>
    <div class="textcontent col-5">
      <h1>Ota yhteyttä!</h1>
      <p>
        Mikäli sinulle ilmenee kysymyksiä, tai haluat antaa jonkinlaista palautetta, otathan meihin yhteyttä:
      </p>
      <p>
        Sähköposti: <a href="#"> officehealth@email.com</a>
      </p>
      <p>
        Pyrimme vastaamaan kysymyksiin 24:n tunnin sisällä.
      </p>
    </div>
  </div>
<footer>
<?php
    //Footer
    include("includes/footer.php");
?>
</footer>
</body>
</html>
