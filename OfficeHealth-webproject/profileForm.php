<?php
session_start();
include_once('config/config.php');

?>
<!-- Profiilitietojen muokkauslomake -->
<div class="profileForm">
<h1>Muokkaa profiilitietojasi</h1>
<fieldset>
<form method="post" id="register">
<p>Käyttäjänimi *
  <br /> <input type="text" name="givenUsername" placeholder="Käyttäjänimi vähintään 4 kirjainta" maxlength="40" pattern="[a-zA-Z]{4,}"/>
  </p><p>
  Ikä *
  <br />  <input type="text" name="givenAge" placeholder="Kelpaava ikä" maxlength="2"/>
  </p><p>
    Pituus (cm) *
    <br />  <input type="text" name="givenHeight" placeholder="Kelpaava pituus (CM)" maxlength="3"/> <!-- Laitetaan nyt senttimetreinä, mutta vaihdetaan mahdollisesti kilogrammoiksi BMI -->
  </p><p>
    Paino (kg) *
    <br />  <input type="text" name="givenWeight" placeholder="Kelpaava paino (KG)" maxlength="3"/>
  </p><p>
    Sukupuoli *
    <br />
    <select class="sukupuoli" name="givenGender" form="register">
      <option value="mies">Mies</option>
      <option value="nainen">Nainen</option>
      <option value="muu">Muu</option>
    </select>
  </p><p>
  Vaihda salasana *
  <br />  <input type="password" name="givenPassword" placeholder="Salasana vähintään 8 merkkiä" maxlength="40"/>
  </p><p>
  Salasana uudelleen *
  <br />  <input type="password" name="givenPasswordVerify" placeholder="Salasana uudelleen" maxlength="40"/>
  </p>
  <p>* = Pakollinen kenttä</p>
  <p>
  <br />  <input type="submit" name="submitUser" value="Päivitä tiedot"/></input>
  </p>
</form>
</fieldset>
</div>

  <?php
  //Lomakkeen submit painettu?
  if(isset($_POST['submitUser'])) {
    // Updaten validointi
    if(empty($_POST['givenUsername'])) {
       $_SESSION['swarningInput']="Nimikenttä ei saa olla tyhjä";
    } else if (strlen($_POST['givenAge']) < 1) {
      $_SESSION['swarningInput']="Liian nuori käyttäjä";
    } else if (strlen($_POST['givenHeight']) < 3) {
      $_SESSION['swarningInput']="Henkilön pituus on väärin";
    } else if (strlen($_POST['givenWeight']) < 2) {
      $_SESSION['swarningInput']="Paino on väärin";
    } else if (empty($_POST['givenGender'])) {
      $_SESSION['swarningInput']="Sukupuolikenttä ei saa olla tyhjä";
    } else if (strlen($_POST['givenPassword']) < 8) {
      $_SESSION['swarningInput']="Salasanan pitää olla vähintään 8 merkkiä";
    } else if ($_POST['givenPassword'] != $_POST['givenPasswordVerify']) {
      $_SESSION['swarningInput']="Salasanat eivät täsmää";
    } else {
    unset($_SESSION['swarningInput']);
    try {
    //***Username ei saa olla käytetty aiemmin
    $data1['name'] = $_SESSION['suserName'];
    $sql1 = "SELECT userID FROM officehealth_user where userName = :name";
    $kysely=$DBH->prepare($sql1);
    $kysely->execute($data1);
    $tulos=$kysely->fetch();
    $currentUserID=$tulos['userID'];
    if($tulos != NULL) { //username ei ole käytössä
      $added='#â‚¬%&&/'; //suolataan annettu salasana
      $data = [
        'name' => $_POST['givenUsername'],
        'height' => $_POST['givenHeight'],
        'weight' => $_POST['givenWeight'],
        'age' => $_POST['givenAge'],
        'gender' => $_POST['givenGender'],
        'pwd' => password_hash($_POST['givenPassword'].$added, PASSWORD_BCRYPT),
        'id' => $currentUserID,
      ];
      $STH = $DBH->prepare("UPDATE officehealth_user SET userName=:name, userHeight=:height, userWeight=:weight, userAge=:age, userGender=:gender, userPass=:pwd WHERE userID =:id");
      $STH->execute($data);
      echo "<script>
      alert('Tiedot päivitetty onnistuneesti, kirjauduthan sisään uudelleen.');
      window.location.href='logout.php';
      </script>";; //Palataan kirjautumissivulle uloskirjautuneena, kun käyttäjätunnusten päivittäminen onnistuu
    }else{
      $_SESSION['swarningInput']="Käyttäjänimi on jo käytössä";
    }
    } catch(PDOException $e) {
    file_put_contents('log/DBErrors.txt', 'profileForm.php: '.$e->getMessage()."\n", FILE_APPEND);
    $_SESSION['swarningInput'] = 'Database problem';
    }
  }
 }
  ?>

  <?php
    //Näytetäänkö lomakesyötteen aiheuttama varoitus?
  if(isset($_SESSION['swarningInput'])){
    echo("<p class=\"warning\">Virhe: ". $_SESSION['swarningInput']."</p>");

  }
  ?>
