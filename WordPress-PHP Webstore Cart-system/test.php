<?php
session_start();
if(isset($_POST['submit'])) {
  global $wpdb;
  // Taking the user input into variables
  $ordernumber = $_POST['ordernmbr'];
  $orderfirstname = $_POST['firstname'];
  $orderpostnumber = $_POST['postnmbr'];
  $page = $_POST['page'];
  // Sanitizing
  $ordernumber = stripslashes_deep($ordernumber);
  $orderfirstname = stripslashes_deep($orderfirstname);
  $orderpostnumber = stripslashes_deep($orderpostnumber);

  // Query that searches for order data from db
  $sql = $wpdb->prepare("SELECT post_id FROM wp_postmeta
  WHERE post_id = %d AND meta_key in ('_billing_first_name', '_billing_postcode')
  and meta_value in ('%s', '%d')

  group by post_id", $ordernumber, $orderfirstname, $orderpostnumber);

  $res = $wpdb->get_results($sql, ARRAY_A);
  $rows = $wpdb->num_rows;
  if( $rows == 0 ) {
    // Edit form2 name later for more fitting description
    wp_redirect('index.php/form/form2');
  }
}
?>
