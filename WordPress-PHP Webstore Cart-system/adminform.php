<?php
//Takes away unnecessary notices
error_reporting(0);
/* Template Name: AdminPage */
// Sets the timezone for the dates
date_default_timezone_set('Europe/Helsinki');
// Gets the theme header
get_header()
?>
<script>
function confirmation() {
  var con = confirm("Oletko varma että haluat hyväksyä pyynnön?");
  if (con == true) {
    return true;

  } else {
    return false;
  }
}
function confirmationtwo() {
  var con = confirm("Oletko varma että haluat hylätä pyynnön?");
  if (con == true) {
    return true;

  } else {
    return false;
  }
}
</script>

<!DOCTYPE html>
<html>
<head>
  <meta charset ="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
@media only screen and (min-width: 1601px) and (max-width: 1920px) {
  .content {
    width: 100%;
    margin-bottom: 3%;
  }
}
@media only screen and (min-width: 1301px) and (max-width: 1600px) {
  html, body {
    height: 100%;
    clear: both;
    margin: 0;
  }

  .content {
    width: 100%;
  }

  .requests {
    width: 100%;
  }
}
@media only screen and (min-width: 1300px) {
  * {
    margin: 0;
    padding: 0;
  }

  html, body {
    height: 100%;
    clear: both;
    margin: 0;
  }

  h1 {
    text-align: center;
    text-decoration: underline;
    margin-bottom: 3%;
  }

  li {
    list-style: none;
  }

  .right {
    float: right;
  }

  .requests {
    background-color: #f7f7f7;
    border: 0.7px solid black;
    margin-bottom: 2.5%;
    padding: 2.5%;
    padding-bottom: 3%;
  }

  .refunds {
    background-color: none;
    float: none;
  }

  .refunds2 {
    background-color: none;
    float: none;
  }

  .refundaccepts {
    background-color: none;
    float: right;
    clear: both;
    margin-bottom: 5%;
  }

  .content {
    border: 1px solid grey;
    float: left;
    padding: 2.5%;
  }

  .impr {
    margin-right: 2%;
    font-size: 5em;
  }
}
@media only screen and (min-width: 802px) and (max-width: 1299px) {
  * {
    margin: 0;
    padding: 0;
  }

  html, body {
    height: 100%;
    clear: both;
    margin: 0;
    overflow: none;
  }

  h1 {
    text-align: center;
    text-decoration: underline;
    margin-bottom: 3%;
  }

  li {
    list-style: none;
  }

  .right {
    float: right;
  }

  .requests {
    display: inline-block;
    word-wrap: break-word;
    background-color: white;
    width: 100%;
    border: 0.7px solid black;
    margin-bottom: 2.5%;
    padding: 2.5%;
  }

  .refunds {
    background-color: none;
    float: none;
  }

  .refundaccepts {
    background-color: none;
    float: right;
    clear: both;
    margin-bottom: 5%;
  }

  .content {
    border: 1px solid grey;
    padding: 2.5%;
    width: 100%;
  }

  .impr {
    margin-right: 2%;
    font-size: 5em;
  }
}
@media only screen and (min-width: 400px) and (max-width: 801px) {
  * {
    margin: 0;
    padding: 0;
  }

  html, body {
    height: 100%;
    clear: both;
    margin: 0;
    overflow: none;
  }

  h1 {
    text-align: center;
    text-decoration: underline;
    margin-bottom: 3%;
  }

  li {
    list-style: none;
  }

  .right {
    float: right;
  }

  .requests {
    display: inline-block;
    word-wrap: break-word;
    background-color: white;
    width: 100%;
    border: 0.7px solid black;
    margin-bottom: 2.5%;
    padding: 2.5%;
  }

  .refunds {
    background-color: none;
    float: none;
  }

  .refundaccepts {
    background-color: none;
    float: right;
    clear: both;
    margin-bottom: 5%;
  }

  .content {
    border: 1px solid grey;
    padding: 2.5%;
    width: 100%;
  }

  .impr {
    margin-right: 2%;
    font-size: 5em;
  }
}

@media only screen and (max-width: 400px) {
  * {
    margin: 0;
    padding: 0;
  }

  html, body {
    height: 100%;
    clear: both;
    margin: 0;
    overflow: none;
  }

  h1 {
    text-align: center;
    text-decoration: underline;
    margin-bottom: 3%;
  }

  li {
    list-style: none;
  }

  .right {
    float: right;
  }

  .requests {
    display: inline-block;
    word-wrap: break-word;
    background-color: white;
    width: 100%;
    border: 0.7px solid black;
    margin-bottom: 2.5%;
    padding: 2.5%;
  }

  .refunds {
    background-color: none;
    float: none;
  }

  .refundaccepts {
    background-color: none;
    float: right;
    clear: both;
    margin-bottom: 5%;
  }

  .content {
    border: 1px solid grey;
    padding: 2.5%;
    width: 100%;
  }

  .impr {
    margin-right: 2%;
    font-size: 5em;
  }
}
  </style>
</head>
<body>
  <p class="impr">Lajittele:</p>
<form id="order" action="" method="post">
  <select id="orderby" name="orderby">
    <option value="accepted">Hyväksytyt</option>
    <option value="deny">Hylätyt</option>
    <option value="waiting">Odottavat</option>
    <input type="submit" name="orderrequests" value="Päivitä"/>
  </select>
</form>
<div class="content">
  <h1>Palautuspyynnöt</h1>
  <p style="text-align:center">Voit hyväksyä ja hylätä odottavia pyyntöjä valitsemalla
    listasta "Odottavat"</p>
  <?php
  // Shows the denied request, but WIP; should probably be order id instead of request id
  /*if(isset($_GET['deleteid'])) {
    $delete = $_GET['deleteid'];
    echo "Eväsitte pyynnön numero $delete";
  }
  // Shows the accepted request, but WIP; should probably be order id instead of request id
  if(isset($_GET['acceptid'])) {
    $accept = $_GET['acceptid'];
    echo "Hyväksyitte pyynnön numero $accept";
  }*/

  //Queries for putting the requests in an order
  // Gets all of the data from db regardless of their order
  global $wpdb;
  $requests = $wpdb->get_results("SELECT * FROM wp_refundrequests LIMIT 10", ARRAY_A);

  if(!isset($_POST['orderrequests'])) {
  foreach ($requests as $row) {
    $custname = $row['order_id'];
    $products = explode('","', $row['product_name']);
    // Select customer names from the db
    $custfullname = $wpdb->get_results("SELECT * FROM wp_postmeta
    WHERE post_id = $custname AND meta_key in ('_billing_first_name', '_billing_last_name')", ARRAY_A);

    $date = date('d-m-Y H:i:s', strtotime($row['request_date'])); //date format
    // Echo all the information from the wp_refundrequests database table as a table
    echo "<ul>" . "<div class='requests'>" . "<li class='refunds'>" . "Palauttajan nimi: ";
    foreach ($custfullname as $var) {
      // Get customer names
      echo " ".$var['meta_value'];
    }
    // Get products
    echo "</br><div style='float:left;font-weight:bold;'>Palautettavien tuotteiden nimet: </br>";
    foreach ($products as $product) {
      $product = trim($product);
      echo "<p>" . $product . "</br></p>";
    }
    echo "</div><p style='clear:both;'><br>Tilauksen numero: ".
    $row['order_id'] . "<br>" . "<br>Kommentti: " . $row['comment'] .
    "<br>" . "Päivämäärä: " . $date . "<br> " . "Tila: " . $row['state'] . " " . "</div>" . "</ul>";
  }
} if(isset($_POST['orderrequests'])) {
// Order by status; the request was accepted
  if ($_POST['orderby'] == 'accepted') {
    $oldest = $wpdb->get_results("SELECT * FROM wp_refundrequests WHERE state = 'Hyväksytty' ORDER BY
    request_date ASC", ARRAY_A);
    foreach ($oldest as $row) {
      $custname = $row['order_id'];
      $date = date('d-m-Y H:i:s', strtotime($row['request_date'])); //date format
      // Select customer names from the db
      $custfullname = $wpdb->get_results("SELECT * FROM wp_postmeta
      WHERE post_id = $custname AND meta_key in ('_billing_first_name', '_billing_last_name')", ARRAY_A);
      // Echo all the information from the wp_refundrequests database table as a table
      echo "<ul>" . "<div class='requests'>" . "<li class='refunds'>" . "Palauttajan nimi: ";
      foreach ($custfullname as $var) {
        // Get customer names
        echo " " . $var['meta_value'];
      }
      echo "<br>";
      // Explode the array so they can be set to new lines each
      $products = explode('","', $row['product_name']);
      // Get products
      echo "<div style='float:left;font-weight:bold;'>Palautettavien tuotteiden nimet: </br>";
    foreach ($products as $product) {
        $product = trim($product);
        echo "<p>" . $product . "</br></p>";
      }
      // Get other info
      echo "</div><br>" .
      "<p style='clear:both;'><br>Tilauksen numero: " . $row['order_id'] . "<br>" . "<br>Kommentti: " . $row['comment'] .
      "<br>" . "Päivämäärä: " . $date . "<br> ". "Tila: " . $row['state'] . "</p></div>" . "</ul>";
    }
    // Order by status; the request was denied
  } if ($_POST['orderby'] == 'deny')  {
    $newest = $wpdb->get_results("SELECT * FROM wp_refundrequests WHERE state = 'Hylätty' ORDER BY
    request_date DESC", ARRAY_A);
    foreach ($newest as $row) {
      $custname = $row['order_id'];
      $date = date('d-m-Y H:i:s', strtotime($row['request_date'])); //date format
      // Select customer names from the db
      $custfullname = $wpdb->get_results("SELECT * FROM wp_postmeta
      WHERE post_id = $custname AND meta_key in ('_billing_first_name', '_billing_last_name')", ARRAY_A);
      // Echo all the information from the wp_refundrequests database table as a table
      echo "<ul>" . "<div class='requests'>" . "<li class='refunds'>" . "Palauttajan nimi: ";
      foreach ($custfullname as $var) {
        // Get customer names
        echo " " . $var['meta_value'];
      }
      echo "<br>";
      // Explode the array so they can be set to new lines each
      $products = explode('","', $row['product_name']);
      // Get products
      echo "<div style='float:left;font-weight:bold;'>Palautettavien tuotteiden nimet: </br>";
    foreach ($products as $product) {
        $product = trim($product);
        echo "<p>" . $product . "</br></p>";
    }
    echo "</div><br>" .
    "<p style='clear:both;'><br>Tilauksen numero: " . $row['order_id'] . "<br>" . "<br>Kommentti: " . $row['comment'] .
    "<br>" . "Päivämäärä: " . $date . "<br> ". "Tila: " . $row['state'] . "</p></div>" . "</ul>";
    }
    // Order by status; the request is still waiting for action
  } if ($_POST['orderby'] == 'waiting')  {
      $newest = $wpdb->get_results("SELECT * FROM wp_refundrequests WHERE state = 'Odottavat' ORDER BY
      request_date DESC", ARRAY_A);
      foreach ($newest as $row) {
      $custname = $row['order_id'];
      $date = date('d-m-Y H:i:s', strtotime($row['request_date'])); //date format
      // Select customer names from the db
      $custfullname = $wpdb->get_results("SELECT * FROM wp_postmeta
      WHERE post_id = $custname AND meta_key in ('_billing_first_name', '_billing_last_name')", ARRAY_A);
      // Echo all the information from the wp_refundrequests database table as a table
      echo "<ul>" . "<div class='requests'>" . "<li class='refunds'>" . "Palauttajan nimi: ";
      foreach ($custfullname as $var) {
        // Get customer names
        echo " " . $var['meta_value'];
      }
      echo "<br>";
      // Explode the array so they can be set to new lines each
      $products = explode('","', $row['product_name']);
      // The products
      echo "<div style='float:left;font-weight:bold;'>Palautettavien tuotteiden nimet: </br>";
    foreach ($products as $product) {
        $product = trim($product);
        echo "<p>" . $product . "</br></p>";
    }
        echo "</div><br>" .
        "<p style='clear:both;'><br>Tilauksen numero: " . $row['order_id'] . "<br>" . "<br>Kommentti: " . $row['comment'] .
        "<br>" . "Päivämäärä: " . $date . "<br> ". "Tila: " . $row['state'] . " " .
        // The accept button that uses the get-parameter to change the status of the request to accepted
        "<a style='float:right;' href='admin-page?acceptid=" . $row['request_id'] .
        "' onclick='return confirmation()'>Hyväksy</a></li>" .
        // The decline button that takes a get-parameter from clicking that changes the status to denied
        "<li><a class='right' href='admin-page?deleteid=" . $row['request_id'] . "' onclick='return confirmationtwo()'>Hylkää</a>" . "</div>" . "</ul>";
      }
    }
  }

  // Adds the accepted request values to a new table. WIP

  if(isset($_GET['acceptid'])) {
    // Set the GET-parameter to a new variable
    $accept = $_GET['acceptid'];
    // Get the order_id, which is the same as the post_id for the right request
    // And set it to a Session variable
    $getid = $wpdb->get_results("SELECT request_id, order_id FROM wp_refundrequests
    WHERE request_id = $accept", ARRAY_A);
    foreach ($getid as $id) {
      $_SESSION['orderid2'] = $id['order_id'];
      echo $id['order_id'];
    }
    // Use the order_id variable from earlier query to get the correct email.
    $orderID = $_SESSION['orderid2'];
    $query = $wpdb->get_results("SELECT meta_key, meta_value FROM wp_postmeta
    WHERE post_id = $orderID AND meta_key in ('_billing_email')", ARRAY_A);
    // Doesn't set the values right, the session variables are undefined.
        foreach ($query as $var) {
          // Get customer names
          echo "Täällä: " . $var['meta_key'] . " JA: " . $var['meta_value'];
          $_SESSION['email2'] = $var['meta_value'];
        }
    // Update the database to change the state to "Hyväksytty"
  $accreq = $wpdb->update('wp_refundrequests',
  array(
          'state' => 'Hyväksytty'
  ),
  array( 'request_id' => $accept),
  array(
          '%s'
  ));
  // Get the info for email and send it
  // User posted variables
  $name = "Viesti";
  $email = "janmikael.buben@gmail.com";
  $message = "Hei! Olemme hyväksyneet palautuksesi tuotteistasi.";

// Php mailer variables
  $to = $_SESSION['email2'];
  $subject = "Hyväksyimme palautuspyyntösi.";
  $headers = 'From: '. $email . "\r\n" .
    'Reply-To: ' . $email . "\r\n";
// Send email with the correct data
// Here put your Validation and send mail
$sent = wp_mail($to, $subject, strip_tags($message), $headers);
// Check if the email was sent and send an error message according to the scenario
      if($sent) {
        echo "Viesti lähetetty onnistuneesti!";
        ?> <script>window.location = window.location.pathname;</script> <?php
      }//message sent!
      else  {
        echo "Jokin meni vikaan lähettäessä viestiä.";
      }//message wasn't sent
  }

  // Deletes wanted refund request from the list using the ID of the request
  if(isset($_GET['deleteid'])) {
    // Get-parameter in to a new variable to use it later
    $delete = $_GET['deleteid'];
    // Get the order_id, which is the same as the post_id for the right request
    // And set it to a Session variable
    $getid = $wpdb->get_results("SELECT request_id, order_id FROM wp_refundrequests
    WHERE request_id = $delete", ARRAY_A);
    foreach ($getid as $id) {
      $_SESSION['orderid'] = $id['order_id'];
    }
    // Use the post_id/order_id to get the correct email and send them a message
    $orderID = $_SESSION['orderid'];
    $query = $wpdb->get_results("SELECT meta_key, meta_value FROM wp_postmeta
    WHERE post_id = $orderID AND meta_key in ('_billing_email')", ARRAY_A);
    // Doesn't set the values right, the session variables are undefined.
        foreach ($query as $var) {
          // Set email into session
          $_SESSION['email'] = $var['meta_value'];
        }
    // Changes the status of the request to "Hylätty"
    $denreq = $wpdb->update('wp_refundrequests',
    array(
            'state' => 'Hylätty'
    ),
    array( 'request_id' => $delete),
    array(
            '%s'
    ));
    // Email information and sending the email
    // User posted variables
    $name = "Viesti";
    $email = "janmikael.buben@gmail.com";
    $message = "Hei! Valitettavasi palautuspyyntösi hylättiin. Otathan meihin yhteyttä, jos teillä on kysymyksiä.";

  // Php mailer variables
    $to = $_SESSION['email'];
    $subject = "Hylkäsimme palautuspyyntösi.";
    $headers = 'From: '. $email . "\r\n" .
      'Reply-To: ' . $email . "\r\n";

  // Check if the email was sent and send an error message according to the scenario
  $sent = wp_mail($to, $subject, strip_tags($message), $headers);
        if($sent) {
          echo "Viesti lähetetty onnistuneesti!";
          ?><script>window.location = window.location.pathname;</script><?php
        }//message sent!
        else  {
          echo "Jokin meni vikaan lähettäessä viestiä.";
        }//message wasn't sent
  }
?>
</body>
</html>

<?php
// Gets the theme footer
  get_footer();
?>
