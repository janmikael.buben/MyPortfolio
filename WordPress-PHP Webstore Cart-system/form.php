<?php
//setting time to UTC+03:00
date_default_timezone_set('Europe/Helsinki');
//starts the session needed for later
session_start();
//includes a secondary form used in the last page
include 'form3.php';
if(isset($_POST['submit'])) {
  global $wpdb;
  // Taking the user input into variables
  $ordernumber = $_POST['ordernmbr'];
  $orderfirstname = $_POST['firstname'];
  $orderpostnumber = $_POST['postnmbr'];
  $page = $_POST['page'];
  // Sanitizing
  // Query that searches for order data from db
  $ordernumber = stripslashes_deep($ordernumber);
  $orderfirstname = stripslashes_deep($orderfirstname);
  $orderpostnumber = stripslashes_deep($orderpostnumber);
  // Checks that the user inputted order number, name and postcode are correct and then redirects them to the next page
  $args = array(
    'id' => $ordernumber,
    'billing_first_name' => $orderfirstname,
    'billing_postcode' => $orderpostnumber,
  );
  $orders = wc_get_orders( $args );
  $rows = $wpdb->num_rows;
  if( $rows == 0 ) {
    // Edit form2 name later for more fitting description
    wp_redirect('index.php/form/form2');
  }
}
//checks if submit is pressed if so, makes new variables with the info from before for later usage
if(isset($_POST['submit'])) {
  if(isset($_POST['ordernmbr']) && $_POST['ordernmbr'] != "") {
    $_SESSION['order'] = $_POST['ordernmbr'];
    $_SESSION['name'] = $_POST['firstname'];
    $_SESSION['post'] = $_POST['postnmbr'];
  }
}
//shows input for debugging
if( isset($_SESSION['order']) ) {
   echo "Asetettu tunnus: " . $_SESSION['order'] . $_SESSION['name'] . $_SESSION['post'];
}
?>
<?php
// Gets the theme header
get_header();
?>
<?php
//Displays the first page of the form
if ($page == 0) {
  ?>
<!DOCTYPE html>
<html>
<head>
 <meta charset ="UTF-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="stylesheet" type="text/css" href="/wordpress/wp-content/themes/responsive-ecommerce/text.css">
</head>
<body>
<div class="Etusivu">
   <form action="" method="post" id="orderinfo">
       <fieldset><legend><b>Tuotteiden palautus</b></legend>
         <div class="step">
       <legend class="sivuluku">Askel 1/3</legend>
     </div>
       <p class="text">Tällä sivulla voi hakea aikaisempia tilauksiaan tiedoilla</p>
       <hr>
       <label>Tilausnumero:</label>
       <div class="textright">
         <p>Tilauksen suorituksen jälkeen annettu numero</p>
       </div>
       <br />
       <input type="text" placeholder="Tilausnumero" name="ordernmbr" required>
       <br />
       <label>Etunimi:</label>
       <div class="textright">
         <p>Tilauksen yhteydessä käytetty etunimi</p>
       </div>
       <br />
       <input type="text" placeholder="Etunimi" name="firstname" required>
       <br />
       <label>Postinumero:</label>
       <div class="textright">
         <p>Tilauksen yhteydessä käytetty postinumero</p>
       </div>
       <br />
       <input class="amountone" type="number" placeholder="Postinumero" name="postnmbr" required>

       <div class="clearfix">
         <input type="hidden" name="page" value="1">
         <input type="submit" class="signupbtn" name="submit" value="Seuraava">
       </div>
    </div>
   </fieldset>
   </form>
</body>
</html>
<?php
// Shows the next page content if the value of "page" is one and the rows
// printed from the first query are more than 0
}  if ($page == 1 && $rows > 0) {
    ?>
    <!DOCTYPE html>
    <html>
    <head>
    <meta charset ="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
    @media only screen and (min-width: 1600px) {
      html, body {
        height: 100%;
        width: 100%;
        clear: both;
        margin: 0;
      }

      .Etusivu {
        width: 100%;
      }

      .chkbx {
        float: left;
      }

      .margin {
        margin-bottom: 10%;
      }
      .important {
        font-weight: bold;
        text-decoration: underline;
      }

      input[type="submit"] {
        background-color: #cea525;
        color: white;
        padding: 1.5%;
        float: right;
      }
    }

    @media only screen and (max-width: 400px) {
      * {
        margin: 0;
        padding: 0;
      }

      html, body {
        height: 100%;
        clear: both;
        margin: 0;
      }

      .Etusivu {
        height: 100%,
      }

      .chkbx {
        float: left;
      }

      .important {
        font-weight: bold;
        text-decoration: underline;
      }

      input[type="submit"] {
        background-color: #cea525;
        color: white;
        padding: 1.5%;
      }
    }
    </style>
    </head>
    <body>
    <div class="Etusivu">
        <form action="" method="post" style="border:0px solid #ccc">
            <fieldset><legend><b>Tuotteiden palautus</b></legend>
              <div class="step">
            <legend>Askel 2/3</legend>
          </div>
          <br />
              <p class="important">Tilauksen tiedot</p>
              <br />
              <div class="valitse">

            <p class="important">Valitse kaikki tuotteet, jotka haluat palauttaa</p>
          </div>
            <hr>

            <script>
            //function for making a checkbox to check all checkboxes
            function toggle(source) {
              jQuery( ':checkbox[name="productinfo[]"]' )
              .prop( 'checked', source.checked );
            }


            </script>

              <input type='checkbox' onClick='toggle(this)' />

            <p class="selectall">Valitse kaikki tuotteet</p>

            <p>Tilauksessa tulleet tuotteet:</p>
            <br />


            <?php
            //Gets and displays data based on certain variables from the database, connects quantity and product name into one string in array
            $order = wc_get_order( $ordernumber );

            foreach ($order->get_items() as $i => $item ){
              $quant = $item->get_quantity();
              $unitprice = $item->get_total() / $item->get_quantity();
              // inputs order data inside a checkbox so it can be taken to the following page
              echo "<input type='checkbox' name='productinfo[]' value='" . " " . // wrapped
              $item->get_name() . " | " . $quant . " | " . $item->get_total() ."; $i'>";

              echo '<p>';
              echo __('Tuotteen nimi: ' ) . $item->get_name() . '<br>';
              // if there's more than 1 unit of the ordered product, displays a number quantity selector with the order original amount as maximum amount
              if($item->get_quantity() > 1) {
                echo "Määrä: " . "<input type='number' name='numberqty[" . $i . "]' value='" // wrapped
                . $quant . "'max='" . $quant . "' min='1' > " . "<br/>";
              }
              else {
              echo __('Määrä: ' ) . $item->get_quantity() . '<br>';
            }
            if ($item->get_quantity() > 1) {

              echo __('Tuotteen kappalehinta: ') . $unitprice . '€' . '<br/>';
            }
              echo __('Tuotteiden kokonaishinta: ' )  . wc_price($item->get_total()) . '</p>' . '<br/>';

           }
            echo '<p>'. __('Tilauksen yhteishinta: ') . $order->get_total() . '</p>';

            ?>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


            <input type="submit" id='check' value="Seuraava"/>
            <script>
            //Checks if any checkboxes have been checked, if not, displays an error
            function checkBoxCheck() {
              if($('input[name="productinfo[]"]:checked').length) {
                console.log("at least one checked");
                return true;
              }
              else {
                alert("Valitse vähintään yksi tuote.");
                return false;
              }
            }
            //runs the script for checking the checkboxes
            $('#check').on('click', checkBoxCheck);
          </script>



            <div class="clearfix">
              <input type="hidden" name="page" value="2">

            </div>
          </fieldset>
        </form>
        <br />
    </div>
    </body>
    </html>
    <?php
    //checks if the 2nd page submit button has been pressed successfully, if so continues to the next page
  } if ($page == 2) {
    global $wbdb;
    //prints errors, for debugging
    if($wpdb->last_error !== ''){
      $wpdb->print_error();
      //creates a new variable for later input of a comment within a textarea
      $comments = $_SESSION['commentone'] = $_POST['comments'];
  }
?>
    <!DOCTYPE html>
    <html>
    <head>
    <meta charset ="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
    @media only screen and (min-width: 1600px) {
      input[type=submit] {
        background-color: #cea525;
        color: white;
        padding: 1%;
        margin-right: 5%;
        float: right;
      }

      .important {
        text-decoration: underline;
        font-weight: bold;
      }
    }

    @media only screen and (max-width: 400px) {
      .margin {
        margin-bottom: 5%;
      }

      input[type=submit] {
        background-color: #cea525;
        color: white;
        padding: 1%;
      }

      .important {
        text-decoration: underline;
        font-weight: bold;
      }

      textarea {
        border: 1px solid black;
        width: 95%;
        resize: none;
      }
    }
    </style>
    </head>
    <body>
    <div class="Etusivu">
        <form action="" method="post" style="border:0px solid #ccc">
            <fieldset><legend><b>Tuotteiden palautus</b></legend>
              <div class="step">
            <legend>Askel 3/3</legend>
          </div>
          <br />
              <p class="important">Palautuksen varmistus</p>
              <br />
              <div class="valitse">
            <p class="important">Haluatko varmasti palauttaa seuraavat tuotteet?</p>
          </div>
            <hr>
            <?php
            $qty_array = isset( $_POST['numberqty'] ) ? (array) $_POST['numberqty'] : array();

            //Makes an array from given data from the previous page
            $test = $_POST['productinfo'];
            //variable for counting total price of the chosen items
            $total2 = 0;
            //variable for counting the total quantity of the selected items
            $totalquantity = 0;
            //Loop for displaying every selected product individually and their prices and quantities and adding each price and quantity to eachother to allow calculation of total price and quantity
            for($i=0; $i < sizeof($test); $i++) {
              list($name, $qunt, $total) = explode("|", $test[$i]);

              // Retrieves the preferred refund quantity.
              list( $total, $index ) = explode( '; ', $total );
              $quantity = isset( $qty_array[ $index ] ) ? intval( $qty_array[ $index ] ) : 1;


              // Updates the quantity in the item *name*.
              $name .= ' , ' . $quantity . ' kpl';
              // First divides the total price by the total amount, then multiplies it by the selected quantity
              $total /= $qunt;
              $total *= $quantity;
              // Echoes the name, quantity is inside the name and total price of the units
              echo "Nimi: ".$name;
              echo "<br>";
              echo "Hinta: ".$total . "€";
              echo "<br>";
              echo "<br/>";
              // Used  for calculating the total refund price
              $total2 += $total;
              // Inserts each name printed by the echoes before and inputs them into an array
              $names[] = $name;
}
            //Uses Json encoding to set the array including the displayed names into session data to allow the displayment of each within the secondary form to insert the data into the database
            $_SESSION['product'] = json_encode($names, JSON_UNESCAPED_UNICODE );
            ?>
            <br />
            <br />
            <h4>Kirjoita alas, miksi haluat palauttaa tuotteen/tuotteet?</h4>
            <?php
            //variable for textarea comment to allow insertion into database
            $comments = $_POST['comments'];

            //Inside textarea $comments displays the data inside it to save it and insert it into database that way?>
            <textarea id='commenter' name='comments' cols='30' rows='4' placeholder='Kirjoita tähän:'><?php echo $comments; ?></textarea>

            <div class="refundprice">
              <?php //inside label is a $total2 variable which is the total price of everything that was checked and taken to this page ?>
            <?php echo '<label>Palautettavien tuotteiden yhteishinta: ' . $total2 . '€' . '</label>'
            ?>
          </div>
            <div class="clearfix">

           <input type="hidden" name="page" value="3">
          <input type="submit" class="signupbtn" name="sendrqst" value="Lähetä">
          </div>
        </fieldset>
        </form>
    </div>
    </body>
    </html>
    <?php

  }

?>
<?php
// Gets the theme footer
get_footer();
?>
